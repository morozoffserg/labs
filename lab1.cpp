#include <string.h>
#include <iostream>
#include "lab1.h"

namespace lab1
{

Album::Album(const char* artist, const char* name, int number, int year)
{
    m_artist = new char[strlen(artist) + 1];
    strcpy(m_artist, artist);
    m_name = new char[strlen(name)+1];
    strcpy(m_name, name);
    m_number = number;
    m_year =  year;
}

Album::Album(char* artist, char* name, int number, int year)
{
    m_artist = new char[strlen(artist) + 1];
    strcpy(m_artist, artist);
    m_name = new char[strlen(name)+1];
    strcpy(m_name, name);
    m_number = number;
    m_year =  year;
}

Album::Album()
{
    m_artist = nullptr;
    m_name = nullptr;
    m_number = 0;
    m_year = 0;
}

Album::~Album()
{
    if(m_artist != nullptr)
        delete[] m_artist;
    if(m_name != nullptr)
        delete[] m_name;
}

int Album::get_number()
{
    return m_number;
}

//функция выделяет память на создание
//строки, указатель на которую возвращает.
//Нужно не забыть потом освободить память
//в функции, которая будет принимать значение.
char* Album::get_name()
{
    char* temp = new char[strlen(m_name) + 1];
    strcpy(temp, m_name);
    return temp;
}

char* Album::get_artist()
{
    char* temp = new char[strlen(m_artist) + 1];
    strcpy(temp, m_artist);
    return temp;
}

void Album::set_number(int number)
{
    m_number = number;
}

void Album::set_year(int year)
{
    m_year = year;
}

void Album::set_name(char* name)
{
    if (name == nullptr)
        return;
    if(m_name != nullptr)
        delete[] m_name;
    m_name = new char[strlen(name) + 1];
    strcpy(m_name, name);

}

void Album::set_artist(char* artist)
{
    if (artist == nullptr)
        return;
    if(m_artist != nullptr)
        delete[] m_artist;
    m_artist = new char[strlen(artist) + 1];
    strcpy(m_artist, artist);
}

void Album::print()
{
    if(m_artist == nullptr)
        std::cout << "Artist: None" << std::endl;
    else
        std::cout << "Artist: " << m_artist << std::endl;
    if(m_name == nullptr)
        std::cout << "Album: None" << std::endl;
    else
        std::cout << "Album: " << m_name << std::endl;
    std::cout << "Number of tracks: " << m_number << std::endl;
    std::cout << "Year: " << m_year << std::endl;
    std::cout << std::endl;
}

}
