#ifndef LAB1_H_INCLUDED
#define LAB1_H_INCLUDED

namespace lab1
{

class Album
{

private:
    char* m_artist ;
    char* m_name;
    int m_number;
    int m_year;

public:
    Album();
    Album(const char* artist, const char* name, int number, int year);
    Album(char* artist, char* name, int number, int year);
    ~Album();

    int get_number();
    int get_year();
    char* get_name();
    char* get_artist();

    void set_number(int number);
    void set_year(int year);
    void set_name(char* name);
    void set_artist(char* artist);
    void print();
//    friend std::ostream &operator<<(std::ostream &stream,const Album& nod);
//    friend std::istream &operator>>(std::istream &stream, Album& nod);
};

//    std::ostream &operator<<(std::ostream &stream,const Album& nod);
//    std::istream &operator>>(std::istream &stream, Album& nod);

}

#endif // LAB1_H_INCLUDED
